import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import * as AWSCognito from "amazon-cognito-identity-js";
/*
  Generated class for the CognitoServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CognitoServiceProvider {
  private userPool: any;
  private userForSignOut: string;
  constructor(public http: HttpClient) {
    this.userPool = new AWSCognito.CognitoUserPool({
      UserPoolId: "us-east-2_Oz3VywzLu",
      ClientId: "5iv8o6s9b0rhb9mu16r6almbhq"
    });
  }

  signUp(phone_number: string, password: string) {
    return new Promise((resolved, reject) => {
      let userAttribute = [];
      // userAttribute.push(
      //   new AWSCognito.CognitoUserAttribute({ Name: "email", Value: email })
      // );
      userAttribute.push(
        new AWSCognito.CognitoUserAttribute({
          Name: "phone_number",
          Value: phone_number
        })
      );
      this.userPool.signUp(
        phone_number,
        password,
        userAttribute,
        null,
        function (err, result) {
          if (err) {
            reject(err);
          } else {
            resolved(result);
          }
        }
      );
    });
  }

  confirmUser(verificationCode, userName) {
    return new Promise((resolved, reject) => {
      const cognitoUser = new AWSCognito.CognitoUser({
        Username: userName,
        Pool: this.userPool
      });

      cognitoUser.confirmRegistration(verificationCode, true, (err,result) => {
      
        if (err) {
          reject(err);
        } else {
          resolved(result);
        }
      });
    });
  }

  confirmUserSMS(userName) {
    return new Promise((resolved, reject) => {
      const cognitoUser = new AWSCognito.CognitoUser({
        Username: userName,
        Pool: this.userPool
      });
      cognitoUser.resendConfirmationCode(function (err, result) {
        if (err) {
          reject(err);
          return;
        }
        resolved(result);
      });
    });
  }

  authenticate(phone_number, password) {
    this.userForSignOut=phone_number;
    return new Promise((resolved, reject) => {
      const authDetails = new AWSCognito.AuthenticationDetails({
        Username: phone_number,
        Password: password
      });

      const cognitoUser = new AWSCognito.CognitoUser({
        Username: phone_number,
        Pool: this.userPool
      });

      cognitoUser.authenticateUser(authDetails, {
        onSuccess: result => {
          resolved(result);
        },
        onFailure: err => {
          reject(err);
        },
        newPasswordRequired: userAttributes => {
          // User was signed up by an admin and must provide new
          // password and required attributes, if any, to complete
          // authentication.

          // the api doesn't accept this field back
          userAttributes.phone_number = phone_number;
          delete userAttributes.phone_number_verified;

          cognitoUser.completeNewPasswordChallenge(password, userAttributes, {
            onSuccess: function (result) { },
            onFailure: function (error) {
              reject(error);
            }
          });
        }
      });
    });
  }

signOut() {
    return new Promise((resolved, reject) => {
      const cognitoUser = new AWSCognito.CognitoUser({
        Username: this.userForSignOut,
        Pool: this.userPool
      });

     if (cognitoUser != null) {
        cognitoUser.signOut();
        resolved();
      }
      

    })
  }

}
