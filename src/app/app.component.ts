import { Component, ViewChild } from '@angular/core';
import { App, Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TranslateService } from '@ngx-translate/core';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { TransactionsPage } from '../pages/transactions/transactions';
import { CustomersPage } from '../pages/customers/customers';
import { AvailableProductsPage } from "../pages/available-products/available-products";
import { AboutUsPage } from '../pages/about-us/about-us'
import { HelpPage } from '../pages/help/help'
import { MyProfilePage } from '../pages/my-profile/my-profile'
import { SettingsPage } from '../pages/settings/settings'
import {VerifyPage} from '../pages/verify/verify'
import { AuthService } from '../services/auth.service';
import { DeveloperActionsPage } from '../pages/developer-actions/developer-actions';
import {CognitoServiceProvider} from '../providers/cognito-service/cognito-service'
import { AppUtilsService } from "../services/utils/app.utils.service";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  //rootPage: any = HomePage;
  //pages;
  rootPage;
  pages: Array<{ title: string, component: any, icon: string }>;
  constructor(app: App,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public auth: AuthService,
    private translateService: TranslateService,
    public Utils: AppUtilsService,
    public cognitoService: CognitoServiceProvider
  ) {

    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'MENU.MY_PROFILE', component: MyProfilePage, icon: 'person' },
      { title: 'MENU.HOME', component: HomePage, icon: 'home' },
      { title: 'MENU.TRANSACTIONS', component: TransactionsPage, icon: 'stats' },
      { title: 'MENU.CUSTOMERS', component: CustomersPage, icon: 'people' },
      { title: 'MENU.PRODUCTS', component: AvailableProductsPage, icon: 'cart' },
      { title: 'MENU.SETTINGS', component: SettingsPage, icon: 'settings' },
      { title: 'MENU.HELP', component: HelpPage, icon: 'help-circle' },
      { title: 'MENU.ABOUT_US', component: AboutUsPage, icon: 'contact' },
      { title: 'MENU.DEVELOPER_ACTIONS', component: DeveloperActionsPage, icon: 'bug' }

    ];

  }

  initializeApp() {

    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.translateService.setDefaultLang('en');
      this.translateService.use('en' )
     
    });
    this.rootPage =LoginPage;//HomePage;//VerifyPage;
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout() {
    this.cognitoService.signOut();
    this.Utils.showToaster("Logged out Successful", 1000, 'green')
    //alert("You are Logged out")
    
    this.nav.setRoot(LoginPage);
  }
}
