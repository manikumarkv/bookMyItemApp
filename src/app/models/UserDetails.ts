export default class UserDetails {
  email: string="";
  password: string;
  address: string = '';
  phonenumber: string;
  name: string = '';
  constructor(phonenumber: string , password: string,email: string=undefined, name: string = undefined, address: string = undefined) {
    this.email = email;
    this.password = password;
    this.name = name;
    this.phonenumber = phonenumber;
    this.address = address;
  }
}
//export const User: UserDetails[] = [{email: '1@g.com', password: '123456'}];