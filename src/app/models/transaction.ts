// import Customer from "./customer";
import { Customer } from "./index";
import ProductItem from "./productItem";
export default class Transaction {
  public id: string;
  public productItems: ProductItem[] = [];
  public paidWith: string;
  public dueAmount: number;
  public customerId: string;
  public customer: Customer;
  public transactionDate: string = "2018-12-21"
  public _id: string;
  public _rev: string;

  constructor() { }

  addProduct(Product: ProductItem): void {
    this.productItems.push(Product);
  }

  get totalAmount(): number {
    let totalAmount = 0;
    this.productItems.map(productItem => {
      totalAmount =
        totalAmount +
        productItem.product.getTotalWithDiscount(productItem.quantity);
    });
    return totalAmount;
  }

  get totalDiscount(): number {
    let totalAmount = 0;
    this.productItems.map(productItem => {
      totalAmount =
        totalAmount + productItem.product.getSavingAmount(productItem.quantity);
    });
    return totalAmount;
  }

  updateCustomer(customer: Customer): void {
    this.customer = customer
  }

  public static Convert(obj: any) {
    let transaction = new Transaction()
    transaction.id = obj.id;
    transaction._rev = obj._rev;
    if(obj.customer != null)
    {
      transaction.updateCustomer(Customer.Convert(obj.customer))
      obj.productItems.map(proItem => {
        let productItem = ProductItem.Convert(proItem)
        transaction.addProduct(productItem)
      })
    }
    else{
      transaction.customer=null
    obj.productItems.map(proItem => {
      let productItem = ProductItem.Convert(proItem)
      transaction.addProduct(productItem)
    })
  }
    return transaction;

  }
}
