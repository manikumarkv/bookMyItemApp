export default class Product {
    public _id: string
    public id: string;
    public name: string;
    public code: string;
    public units: string;
    public unitMrp: number;
    public discount: number;
    public quntiy: number;
    public _rev: string;

    constructor(id: string, name: string, code: string, units: string, unitMrp: number, discount: number, _id: string = id, _rev: string = undefined) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.units = units;
        this.unitMrp = unitMrp;
        this.discount = discount;
        this._id = _id
        this._rev = _rev
    }
    public getTotalWithoutDiscount(quantity: number): number {
        return quantity * this.unitMrp;
    }
    public getTotalWithDiscount(quantity: number): number {
        return quantity * (this.unitMrp - this.discount);
    }
    public getSavingAmount(quantity: number): number {
        return this.getTotalWithoutDiscount(quantity) - this.getTotalWithDiscount(quantity);
    }

    public static Convert(obj: any) {
        return new Product(obj.id, obj.name, obj.code, obj.units, obj.unitMrp, obj.discount, obj._id, obj._rev)
    }
}