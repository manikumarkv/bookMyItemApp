import { NgModule } from '@angular/core';
import { CurrencySymbolComponent } from './currency-symbol/currency-symbol';
@NgModule({
	declarations: [CurrencySymbolComponent],
	imports: [],
	exports: [CurrencySymbolComponent]
})
export class ComponentsModule {}
