import { Component } from '@angular/core';

/**
 * Generated class for the CurrencySymbolComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'currency-symbol',
  templateUrl: 'currency-symbol.html'
})
export class CurrencySymbolComponent {

  symbol: string;

  constructor() {
    this.symbol = '₹';
  }

}
