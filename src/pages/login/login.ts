import { Component } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { NavController } from "ionic-angular";
import { HomePage } from '../home/home';
import { AuthService } from "../../services/auth.service";
import { SignupPage } from '../signup/signup';
import { AppUtilsService } from "../../services/utils/app.utils.service";
import { UserDetails } from '../../app/models';


import { CognitoServiceProvider } from "../../providers/cognito-service/cognito-service";
//import { SignupPage } from "../signup/signup.module"
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
//  email: string;
 // password: string;
  signUpPage = SignupPage;

  loginForm: FormGroup;
  loginError: string;
  constructor(
    private navCtrl: NavController,
    private auth: AuthService,
    fb: FormBuilder,
    public appUtils: AppUtilsService,
  public CognitoSerive:CognitoServiceProvider
  ) {
    this.loginForm = fb.group({
      // email: ["", Validators.compose([Validators.required, Validators.email])],
      phone_number: ['', Validators.compose([Validators.required, Validators.minLength(10)])],
      password: ["", Validators.compose([Validators.required, Validators.minLength(6)])
      ]
    });
  }
  // login() {
  //   let data = this.loginForm.value;

  //   if (!data.email) {
  //     return;
  //   }


  //   const user = new UserDetails(data.email, data.password)
  //   this.auth.Loginlocal(user).then(
  //     succ => {
  //       this.appUtils.showToaster("login success", 1000, 'green')
  //       this.navCtrl.setRoot(HomePage)
  //     })
  //     .catch(err => {
  //       this.appUtils.showToaster("Please check username or password", 3000, 'red')
  //       //alert("Please check username or password")
  //     });

  // }

  signup() {
    this.navCtrl.push(SignupPage);
  }

  loginWithGoogle() {
    this.navCtrl.setRoot(HomePage)
  }

  login(){
    let data = this.loginForm.value;

      if (!data.phone_number) {
        return;
      }
     const user = new UserDetails(data.phone_number, data.password) 
    this.CognitoSerive.authenticate(user.phonenumber,user.password)
    .then(res =>{
      this.appUtils.showToaster("login success", 1000, 'green')
      this.navCtrl.setRoot(HomePage,{userData: res})
      console.log(res);
    }, err =>{
      console.log(err);
      this.appUtils.showToaster("Please check username or password", 3000, 'red')
    });
  }

}
