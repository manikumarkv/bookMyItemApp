import { Component, OnInit } from "@angular/core";
import { NavController,NavParams } from "ionic-angular";
import Product from "../../app/models/product";
import { ProductsService } from "../../services/products.service";
import { Transaction } from "../../app/models";
import ProductItem from "../../app/models/productItem";
import { GenerateBillPage } from "../generate-bill/generate-bill";
import { ToastController } from 'ionic-angular';
import { TranasactionsService } from '../../services/transactions.service';
import { AppUtilsService } from '../../services/utils/app.utils.service';

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage implements OnInit {
  userData: any;
  showContent: boolean = false;
  edit: boolean = false;
  name: string = null;
  product_id: string = null;
  addProduct: Product;
  productItem: any;
  newTransaction: Transaction = new Transaction();
  quantity: number = 0;
  editquantity: number;
  availableProducts: Product[] = [];
  selectedProduct: Product[] = []
  productSearch: boolean = false;
  constructor(
    public navCtrl: NavController,
    public productsService: ProductsService,
    public toastCtrl: ToastController,
    public transactionService: TranasactionsService,
    public util: AppUtilsService,
    public navParams: NavParams
  ) { 
    this.userData = this.navParams.get("userData");
   
  }

  ngOnInit() {
    this.productsService.GetAll().then(succ => {
      this.availableProducts = succ.map(record => {
        return new Product(record.id, record.name, record.code, record.units, record.unitMrp, record.discount, record._id, record._rev)
      });
      // let f = this.availableProducts[0].getTotalWithDiscount(1)
    })

  }

  Cancel()
  {
    this.productSearch = false
    this.addProduct = null
    this.quantity = null;
  }

  addName(val: Product) {
    this.addProduct = val;
    this.productSearch = true
    this.selectedProduct= [];
  }

  getItems(ev) {
    var val = ev.target.value;
    if (val != '') {
      this.selectedProduct = this.availableProducts.filter((item) => {
        return (item.name.indexOf(val) > -1);
      })
    }

  }

  addItem(addNewproduct: Product, quantity: number) {
    this.productItem = new ProductItem(addNewproduct, quantity);
    this.newTransaction.addProduct(this.productItem);
    this.quantity = null;
    this.name = "";
    this.showContent = true;
    this.productSearch = false
  //  this.onCancel();
  }

  generateBill() {
    this.navCtrl.push(GenerateBillPage, {
      transaction: this.newTransaction
    });
    this.newTransaction = new Transaction()
    this.showContent = false;
  }

  removeItem(val) {
    this.newTransaction.productItems.splice(val, 1)
    this.util.showToaster("Item Removed",1000,"");
    // TODO: use utility method
  }

  editItem(item) {
    item.editable = true
    this.editquantity = item.quantity;
  }

  saveItem(item, quantity) {
    item.editable = false
    item.quantity = quantity;

  }
}
