import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { CustomersService } from '../../services/customers.service';
import { TranasactionsService } from '../../services/transactions.service';
import Customer from '../../app/models/customer';
import { ModalController } from 'ionic-angular';
import { SearchcustomerPage } from '../searchcustomer/searchcustomer';
import { Transaction } from "../../app/models";
import ProductItem from '../../app/models/productItem';
import { AppUtilsService } from '../../services/utils/app.utils.service';
import { Guid } from '../../app/models/guid';


@Component({
  selector: "page-generate-bill",
  templateUrl: "generate-bill.html"
})
export class GenerateBillPage {


  public customerlist: Customer[] = [];
  selectedCustomerBool: boolean = true;
  generalbill: boolean = false;

  values1: string = "";
  searchcustomer: Customer = null;
  customer: Customer;
  selectedItems: ProductItem[] = [];
  selectedCustomer: Customer = null;
  transaction: Transaction;
  constructor(public events: Events, public navCtrl: NavController,
    public navParams: NavParams, public customerService: CustomersService,
    public modalCtrl: ModalController,
    public transactionsService: TranasactionsService,
    public appUtilsService: AppUtilsService) {
    this.transaction = navParams.get("transaction");

    events.subscribe('customer: selected', (selectedcustomer) => {
      this.searchcustomer = this.customerService.GetSearchedCustomer();
      if(this.searchcustomer == null){
        this.selectedCustomerBool = true;
      }
    })
    events.subscribe('customer1: notselected', (selectedcustomer) => {
      this.searchcustomer = null;
      if(this.searchcustomer == null){
        this.selectedCustomerBool = true;
      }
    })
   


    if (navParams.get("transaction").productItem != null)
      this.selectedItems = navParams.get("transaction").productItem;

    this.customerService.GetAll().then(succ => {
      this.customerlist = succ;
    })
  }

  presentModal() {
    const modal = this.modalCtrl.create(SearchcustomerPage);
    modal.present();
    this.selectedCustomerBool = false;
    
  }

  changeCustomer() {
    this.selectedCustomerBool = true;
    this.customer = null
  }

  confirmBill() {
    this.transaction.updateCustomer(this.searchcustomer);
    this.transaction.id = Guid.NewGuid()
    this.transaction._id = this.transaction.id
    this.transaction._rev = undefined
    // this.transactionsService.Add(this.transaction)
    this.transactionsService.Add(this.transaction).then(res => {
      this.appUtilsService.showToaster("bill is confirmed", 1000, "")
      this.navCtrl.pop()
    }).catch(err => {
      alert("failed");
    })
  }

  generalBill() {
    this.searchcustomer = null;
    this.generalbill = true;
    this.selectedCustomerBool = false

  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad GenerateBillPage");
  }

}
