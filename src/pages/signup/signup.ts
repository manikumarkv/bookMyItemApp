import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { ModalController , Events} from 'ionic-angular';
import { LoginPage } from '../login/login';
import { AppUtilsService } from "../../services/utils/app.utils.service";
import { UserDetails } from '../../app/models';
import { NavParams, AlertController  } from "ionic-angular";

import { CognitoServiceProvider } from "../../providers/cognito-service/cognito-service";
import { VerifyPage } from '../verify/verify';
@IonicPage()
@Component({
	selector: 'page-signup',
	templateUrl: 'signup.html',
})
export class SignupPage {

	signupError: string;
	form: FormGroup;
	constructor(
		fb: FormBuilder,
		private navCtrl: NavController,
		private auth: AuthService,
		public utils: AppUtilsService,
		public navParams: NavParams,
		public CognitoService: CognitoServiceProvider,
		public alertController: AlertController,
		public modalCtrl: ModalController,
		public events: Events
	) {
		this.form = fb.group({
		//	email: ['', Validators.compose([Validators.required, Validators.email])],
			phone_number: ['', Validators.compose([Validators.required, Validators.minLength(10)])],
			password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
		});


    events.subscribe('customer: selected', (data) => {
     
    })
	}

	signup() {
		let data = this.form.value;

		const user = new UserDetails(data.phone_number, data.password)
		this.auth.setName(user).then(
			succ => {
				this.utils.showToaster("Successfully Signup", 1000, 'green')
				this.navCtrl.setRoot(LoginPage)
				//	alert("Successfully Signup")
			})
			.catch(err => {
				this.utils.showToaster("user already exists", 2000, 'red')
				//alert("user already exists")
			});
	}

	register() {
		let data = this.form.value;
		this.CognitoService.signUp(data.phone_number, data.password).then(
			res => {
				this.changeTo(data);
				//this.verifyUserCode(data);
			},
			err => {
				console.log(err);
			}
		);
	}
 
	changeTo(data) {
    this.navCtrl.push(VerifyPage, {
      data: data
		});
		
  }

	verifyUserCode(userDetails) {
    let alert = this.alertController.create({
      title: "Enter Verfication Code",
      inputs: [
        {
          name: "VerificationCode",
          placeholder: "Verification Code"
        }
      ],
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: data => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Verify",
          handler: data => {
            this.verifyUser(data.VerificationCode,userDetails);
          }
				},
				{
          text: "Verify via SMS",
          handler: data => {
            this.verifyUserSMS(userDetails);
          }
				}
				
      ]
    });
    alert.present();
  }

	verifyUser(verificationCode,userDetails) {
    this.CognitoService.confirmUser(verificationCode, userDetails.phone_number).then(
      res => {
        console.log(res);
      },
      err => {
        alert(err.message);
      }
    );
	}
	verifyUserSMS(userDetails) {
    this.CognitoService.confirmUserSMS(userDetails.phone_number).then(
      res => {
				console.log(res);
				this.verifyUserCode(userDetails);
      },
      err => {
        alert(err.message);
      }
    );
  }
}
