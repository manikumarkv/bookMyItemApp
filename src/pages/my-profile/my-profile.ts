import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthService } from "../../services/auth.service";
import { UserDetails } from '../../app/models';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AppUtilsService } from '../../services/utils/app.utils.service';
import { TranslateService } from '@ngx-translate/core';
/**
 * Generated class for the MyProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-profile',
  templateUrl: 'my-profile.html',
})
export class MyProfilePage {
  user;
  email: string;
  passsword: string;
  name: string;
  phonenumber: string;
  address: string;
  form: FormGroup;
  constructor(public translateService: TranslateService,public navCtrl: NavController, public navParams: NavParams, public auth: AuthService, formbuilder: FormBuilder, public util: AppUtilsService) {
    this.form = formbuilder.group({
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      name: ['',],
      phonenumber: ['',],
      address: ['',]
    });
  }
  ngOnInit() {
    //this.auth.setName();
    // this.auth.getName();
    this.auth.getNameLogin().then(
      (succ) => {
        this.user = new UserDetails(succ.phonenumber, succ.passsword, succ.name, succ.email, succ.address)
        this.email = this.user.email;
        this.name = this.user.name;
        this.passsword = this.user.passsword;
        this.address = this.user.address;
        this.phonenumber = this.user.phonenumber;
      }
    )
  }

  Update() {
    let data = this.form.value;
    data.passsword = this.passsword;
    let newUpdate = new UserDetails(data.phonenumber, data.passsword, data.name, data.email, data.address)
    this.auth.setName(newUpdate).then((succ) => this.util.showToaster("Updated",1800,'green'))

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyProfilePage');
  }

}
