import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProductsService } from '../../services/products.service';
import Product from '../../app/models/product';
import { AppUtilsService } from '../../services/utils/app.utils.service';


@Component({
  selector: 'page-editproduct',
  templateUrl: 'editproduct.html',
})
export class EditproductPage {
  public product: Product[] = [];
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public productservice: ProductsService,
    public appUtilsService: AppUtilsService) {
    this.product = navParams.get("product");
  }

  editProduct(product) {
    this.productservice.Update(product).then(succ => {
      this.appUtilsService.showToaster("product details edited",1000, '')
      this.navCtrl.pop()
    })


  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad EditproductPage');
  }

}
