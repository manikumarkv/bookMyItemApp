import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController , Events, AlertController} from 'ionic-angular';
import {CognitoServiceProvider} from '../../providers/cognito-service/cognito-service'
import { LoginPage } from '../login/login';
import {AppUtilsService} from '../../services/utils/app.utils.service'
/** 
 * Generated class for the VerifyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-verify',
  templateUrl: 'verify.html',
})
export class VerifyPage {
  data: any;
  Code:string;
  constructor(public toaster: AppUtilsService ,public events: Events,public navCtrl: NavController, public navParams: NavParams , public viewctrl: ViewController ,public alertController: AlertController,public CognitoService: CognitoServiceProvider) {
    this.data = navParams.get("data");
   // this.verifyUserCode(this.data);
  } 

	verifyUser() {
    this.CognitoService.confirmUser(this.Code, this.data.phone_number).then(
      res => {
        this.toaster.showToaster("Verification SuccessFul",3000,'green')
        this.navCtrl.push(LoginPage)
        console.log(res);
      },
      err => {
        this.toaster.showToaster("Please Enter the Correct Verification Code",2000,'red')
      }
    );
    // this.navCtrl.pop()
	}
	verifyUserSMS(userDetails) {
    this.CognitoService.confirmUserSMS(userDetails.phone_number).then(
      res => {
				console.log(res);
      },
      err => {
        alert(err.message);
      }
    );
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VerifyPage');
  }

}
