import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProductsService } from '../../services/products.service';
import { Product, Customer } from '../../app/models';
import { Guid } from '../../app/models/guid';
import { CustomersService } from '../../services/customers.service';
import { TranasactionsService } from '../../services/transactions.service';


@Component({
  selector: 'page-developer-actions',
  templateUrl: 'developer-actions.html',
})
export class DeveloperActionsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public productService: ProductsService,
    public customerService: CustomersService,
    public transactions: TranasactionsService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeveloperActionsPage');
  }

  addProducts() {
    const a = Guid.NewGuid()
    const b = Guid.NewGuid()

    const c = Guid.NewGuid()
    const d = Guid.NewGuid()
    const e = Guid.NewGuid()
    this.productService.Add(new Product(a, 'Dosa', 'DOSA', 'piece', 50, 5))
    this.productService.Add(new Product(b, 'Idly', 'DOSA', 'piece', 60, 5))
    this.productService.Add(new Product(c, 'Vada', 'DOSA', 'piece', 70, 5))
    this.productService.Add(new Product(d, 'Upma', 'DOSA', 'piece', 80, 5))
    this.productService.Add(new Product(e, 'Set Dosa', 'DOSA', 'piece', 40, 5))

  }

  deleteAllProducts() {
    this.productService.removeAll()
  }

  addCustomers() {
    this.customerService.Add(new Customer(Guid.NewGuid(), 'Mani', 'Kumar', 121212, 'addres1'));
    this.customerService.Add(new Customer(Guid.NewGuid(), 'Sai', 'Kumar', 121555212, 'addres1'));
    this.customerService.Add(new Customer(Guid.NewGuid(), 'Nagesh', 'Kumar', 6666, 'addres1'));
    this.customerService.Add(new Customer(Guid.NewGuid(), 'yamini', 'Kumar', 8888, 'addres1'));

  }
  deleteCustomers() {
    this.customerService.removeAll()
  }

  deleteTransactions() {
    this.transactions.removeAll()
  }

}
