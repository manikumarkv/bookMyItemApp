import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  setlanguage: string;
  language = [ {value: "hi", name: "Hindi"}, {value: "te", name: "Telugu"},{value: "en", name: "English"}  ];
//selfruits = [this.fruits[1]];
  //currency: any = [];
  // {value: "en", name: "English"},
  // {value: "hi", name: "Hindi"},
  // {value: "te", name: "Telugu"}
  constructor(public navCtrl: NavController, public navParams: NavParams,public translateService: TranslateService) {
   this.setlanguage=this.translateService.currentLang;
    console.log(this.translateService.currentLang)
  }

  ngOnInit() {
   // this.languages = ["English", "Hindi", "Telugu"];
   // this.currency = ["IND", "USD"]

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }
  languageChange()
  {
    console.log(this.setlanguage);
    this.translateService.use(this.setlanguage );
  }

}
