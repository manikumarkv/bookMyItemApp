import { Injectable } from "@angular/core";
import { ProductsDbService } from '../services/dbServices/products.db.service'


@Injectable()
export class AppMetadataService {
    private metada: object

    constructor(public productDbService: ProductsDbService) {

    }
    language() {
        return 'en'
    }
    currency() {
        return 'inr'
    }


}
