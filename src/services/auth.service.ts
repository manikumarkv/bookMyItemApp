import { Injectable } from "@angular/core";
import { UserDetails } from '../app/models/index'
import { rejects } from "assert";
import { Storage } from '@ionic/storage'


@Injectable()
export class AuthService {
  newUsers = [new UserDetails("1234567890", '123456'), new UserDetails("0987654321", "123456")];
  test;
  constructor(public storage: Storage) {
    this.storage = storage;
  }
  update(credentials: UserDetails): Promise<any> {
    var promise = new Promise((resolve, reject) => {
      localStorage.setItem('userLogin', JSON.stringify({
        'phonenumber': credentials.phonenumber,
        'password': credentials.password,
        'name': credentials.name,
        'email': credentials.email,
        'address': credentials.address
      }))
      resolve();
    }).catch(err => {
      rejects(err)
    });
    return promise;
  };

  setName(credentials: UserDetails): Promise<any> {
    var promise = new Promise((resolve, reject) => {
      localStorage.setItem('userLogin', JSON.stringify(credentials))
      resolve();
    }).catch(err => {
      rejects(err)
    });
    return promise;
  };

  getNameLogin(): Promise<any> {
    var promise = new Promise((resolve, reject) => {
      this.test = new UserDetails(JSON.parse(localStorage.getItem('userLogin')).phonenumber,JSON.parse(localStorage.getItem('userLogin')).password, JSON.parse(localStorage.getItem('userLogin')).name, JSON.parse(localStorage.getItem('userLogin')).email,JSON.parse(localStorage.getItem('userLogin')).address)
      resolve(this.test);
    }).catch(err => {
      rejects(err)
    });
    return promise;
  }

  getName() {
    alert("test" + JSON.parse(localStorage.getItem('userLogin')).phonenumber)
  };

  signInWithEmail(credentials) {
    console.log("Sign in with phonenumber");
  }

  signUp(credentials: UserDetails): Promise<any> {
    var promise = new Promise((resolve, reject) => {
      if (this.newUsers.filter(res => (res.phonenumber == credentials.phonenumber)).length == 0) {
        this.newUsers.push(credentials);
        resolve(credentials);
      }
      else {
        reject()
      }
    }).catch(err => {
      rejects(err)
    });
    return promise;
  }


  Login(credentials: UserDetails): Promise<any> {
    var promise = new Promise((resolve, reject) => {
      //var ember = this.newUser.filter(res => (res.email == credentials.User && res.password == credentials.password));
      if (this.newUsers.filter(res => (res.phonenumber == credentials.phonenumber && res.password == credentials.password)).length == 1) {
        resolve(credentials);
      }
      else {
        reject();
      }
    }).catch(err => {
      rejects(err)
    });
    return promise;
  }

  Loginlocal(credentials: UserDetails): Promise<any> {
    var promise = new Promise((resolve, reject) => {
      this.test = new UserDetails(JSON.parse(localStorage.getItem('userLogin')).phonenumber, JSON.parse(localStorage.getItem('userLogin')).password, JSON.parse(localStorage.getItem('userLogin')).name, JSON.parse(localStorage.getItem('userLogin')).email, JSON.parse(localStorage.getItem('userLogin')).address)
      if (this.test.phonenumber == credentials.phonenumber && this.test.password == credentials.password) {
        resolve(credentials);
      }
      else {
        reject();
      }
    }).catch(err => {
      rejects(err)
    });
    return promise;
  }

  get authenticated(): boolean {
    return true
  }

  getEmail() {
  }

  signOut(): void {

  }

  signInWithGoogle() {
    console.log("Sign in with google");

  }

  sendEmail() {

  }
  resetPassword(email: string): void {
  }
}
