
import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import { Transaction } from '../../app/models';
import cordovaSqlitePlugin from 'pouchdb-adapter-cordova-sqlite'


@Injectable()
export class TransactionsDbService {
    private _DB: any;
    constructor() {
        this.initialiseDB();
    }

    initialiseDB() {
        PouchDB.plugin(cordovaSqlitePlugin);
        this._DB = new PouchDB('transactions', { adapter: 'cordova-sqlite' });
    }


    Add(transaction: Transaction): Promise<any> {
        return this._DB.put(transaction)
    }

    // Remove(id): Promise<any> {
    //     return this._DB.remove(id)
    // }

    Update(transaction: Transaction): Promise<any> {
        return this._DB.put(transaction)
    }

    Get(id: string): Promise<any> {
        return this._DB.get(id)
    }

    GetAll(): Promise<any> {
        return this._DB.allDocs({ include_docs: true, })
    }


    remove(transactions: Transaction[]) {
        transactions.map(transaction => {
            transaction["_deleted"] = true
        })
        return this._DB.bulkDocs(transactions)
    }

    removeAll(transaction: Transaction[]): Promise<any> {
        transaction.map(transaction_delete => {
            transaction_delete["_deleted"] = true
        })
        return this._DB.destroy(transaction)
    }

}
