import { Injectable } from "@angular/core";
import Transaction from "../app/models/transaction";
import { TransactionsDbService } from './dbServices/transactions.db.service'
@Injectable()
export class TranasactionsService {
  private transactions: Transaction[] = []
  constructor(public transactionsDbService: TransactionsDbService) { }

  Add(transaction: Transaction): Promise<any> {
    var promise = new Promise((resolve, reject) => {
      this.transactionsDbService.Add(transaction).then(res => {
        this.transactions.push(transaction);
        resolve(transaction)
      }).catch(err => {
        reject(err)
      })
    });
    return promise;
  }

  Remove(id): void {
    this.transactions = this.transactions.filter(product => product.id !== id);
  }

  Update(transaction: Transaction): void {
    this.transactions.map(_transaction => {
      if (_transaction.id === transaction.id) {
        _transaction = transaction;
      }
    });
  }

  Get(id: string): Transaction {
    return this.transactions.filter(transaction => transaction.id == id)[0];
  }

  GetAll(): Promise<any> {
    var promise = new Promise((resolve, reject) => {
      this.transactionsDbService.GetAll().then(succ => {
        this.transactions = succ.rows.map(data => { return data.doc });
        resolve(this.transactions)
      }).catch(err => {
        reject(err)
      })
    });
    return promise;

  }


  removeAll() {
    var promise = new Promise((resolve, reject) => {
      this.transactionsDbService.removeAll(this.transactions).then(res => {
        this.transactions = []
        resolve([])
      }).catch(err => {
        reject(err)
      })
    });
    return promise;
  }
}
