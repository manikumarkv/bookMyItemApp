import { Injectable } from "@angular/core";
import { ToastController } from "ionic-angular";

@Injectable()
export class AppUtilsService {

  constructor(public toster: ToastController) {

  }

  showToaster(message: string, duration: number, color: string): void {
    const toaster = this.toster.create({
      message: message,
      duration: duration,
      position: 'top',
      cssClass: color
    });
    toaster.present();
  }
}
